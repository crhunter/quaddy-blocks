#!/usr/bin/python

'''
    (C) Copyright 2020  Clinton Hunter
    This file is part of Quaddy Blocks

    Quaddy Blocks is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Quaddy Blocks is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Quaddy Blocks.  If not, see <https://www.gnu.org/licenses/>.
'''

import json
import tkinter as tk
import tkinter.font as tkfont


class ScoreFrame:
    def __init__(self, root):
        self.root = root
        self.scoreFile = 'highscores.json'
        self.scoreData = json.loads(open(self.scoreFile, 'r').read())
        self.createWidgets()

    def createWidgets(self):
        headFont = (tkfont.nametofont('TkHeadingFont').configure()['family'], 16, 'bold')
        textFont = (tkfont.nametofont('TkFixedFont').configure()['family'], 14)
        posLabelFont = (textFont[0], 14, 'bold')

        self.frame = tk.Frame(self.root)
        self.scoreFrame = tk.LabelFrame(self.frame, padx=10, pady=10, text='Hall of Fame', font=headFont)
        self.root.resizable(0, 0)
        for y in range(10):
            for x in range(3):
                if x == 0:
                    self.cell = tk.Label(self.scoreFrame, width=3, font=posLabelFont)
                    self.cell.config(text=str(y+1))
                elif x == 1:
                    self.cell = tk.Label(self.scoreFrame, width=12, anchor='w', font=textFont)
                    self.cell.config(text=' ' + self.scoreData[x-1][y])
                elif x == 2:
                    self.cell = tk.Label(self.scoreFrame, width=10, anchor='e', font=textFont)
                    self.cell.config(text='{:,}'.format(self.scoreData[x-1][y]) + ' ')
                self.cell.grid(row=y, column=x)
        self.spacer = tk.Label(self.root)
        self.OkBtn = tk.Button(self.frame, text="OK")
        self.OkBtn.bind('<Button-1>', self.closeWindow)
        self.scoreFrame.grid(column=0, row=0, columnspan=7, padx=20, pady=0, sticky='S')
        self.OkBtn.grid(column=3, row=1, padx=5, pady=15, sticky='N')
        self.spacer.pack()
        self.frame.pack()

    def closeWindow(self, event):
        '''call to exit the application'''
        self.root.destroy()


def openScores():
    root = tk.Tk()
    ScoreFrame(root)
    root.title('High Scores')
    root.mainloop()


if __name__ == '__main__':
    openScores()
