# Wishlist/TO-DO list

* Have game settings update when the configuration is changed rather than having to close/reopen the game.
* Investigate adding other game modes (Sprint etc)

# Update History

## Version 1.4

**Feature Changes:**

* added in-game dialog to change the configuration file (key bindings, textures etc).  Currently requires a restart to load the new configuration.

## Version 1.3

**Bugfixes:**
* corrected alignment issue in help.txt (mixed spaces and tabs)

**Feature Changes:**
* game configuration now stored in a JSON file, and is loaded when game is launched
* a piece can now be held (disabled by default)
* updated help.txt to include new hold piece function

## Version 1.2

**Feature Changes:**
* block textures added (adjustable in source code only)
* scaling at 100%, 150% and 200% (adjustable in source code only)

## Version 1.1

**Bugfixes:**
* Harddrop clipping through pieces to empty space

**Feature Changes:**
* line clear stats
* scoring for perfect clears
* PPS (speed stat - pieces per second)
* Label displayed when game is paused.
* short delay before next piece if not hard or soft dropped

## Version 1.0
 * 1st release
