#!/usr/bin/python

'''
    (C) Copyright 2020  Clinton Hunter
    This file is part of Quaddy Blocks

    Quaddy Blocks is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Quaddy Blocks is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Quaddy Blocks.  If not, see <https://www.gnu.org/licenses/>.
'''

import json
import os
import tkinter as tk
import tkinter.ttk as ttk
import tkinter.messagebox as tkmessagebox


class ConfigFrame:
    def __init__(self, root):
        f = open('config.json', 'r')
        self.cfg = json.load(f)
        self.cfgWidgets = {}
        f.close()
        self.root = root
        self.root.protocol('WM_DELETE_WINDOW', self.closeWindowOk)
        self.root.resizable(0, 0)
        self.createWidgets()

    def createWidgets(self):
        self.frame = tk.Frame(self.root)
        self.bindFrame = tk.LabelFrame(self.frame, padx=10, pady=10, text='Key Bindings')
        currRow = -1
        for i in self.cfg['keybinds']:
            currRow += 1
            kb = self.cfg['keybinds'][i]
            self.cfgWidgets[i] = {}
            self.cfgWidgets[i]['label'] = tk.Label(self.bindFrame, text='(%s)' % (kb['keysym']))
            self.cfgWidgets[i]['label'].grid(row=currRow, column=0, padx=(0, 10))
            label = tk.Label(self.bindFrame, text=kb['desc'])
            label.grid(row=currRow, column=1, sticky="W", padx=(0, 5))
            self.cfgWidgets[i]['btn'] = tk.Button(self.bindFrame, text="Set")
            self.cfgWidgets[i]['btn'].grid(row=currRow, column=2)
            self.cfgWidgets[i]['btn'].bind('<Button-1>', lambda event, keyAction=i: self.setBinding(event, keyAction))

        self.otherFrame = tk.LabelFrame(self.frame, text='Other Settings')
        self.holdCheck = tk.Checkbutton(self.otherFrame, text="Hold Enabled", onvalue=True,
                                        offvalue=False, command=self.toggleHold)
        if self.cfg['enableHold'] is True:
            self.holdCheck.select()
        self.holdCheck.grid(row=0, column=0, columnspan=2, pady=(10, 0), sticky='W')
        label = tk.Label(self.otherFrame, text="Window Scale")
        label.grid(row=1, column=0, padx=10, pady=10)
        self.winScale = ttk.Combobox(self.otherFrame, width=5)
        self.winScale['values'] = [1.0, 1.5, 2.0]
        if self.cfg['scale'] == 1.0:
            self.winScale.current(0)
        elif self.cfg['scale'] == 1.5:
            self.winScale.current(1)
        else:
            self.winScale.current(2)
        self.winScale['state'] = 'readonly'
        self.winScale.grid(row=1, column=1, padx=10, pady=10)
        label = tk.Label(self.otherFrame, text="Block Texture")
        label.grid(row=2, column=0, padx=10, pady=10)

        self.textures = set()
        for fname in os.listdir('.%stex%s' % (os.sep, os.sep)):
            self.textures.add(fname[:-7])
        self.textures = list(self.textures)

        self.blockTex = ttk.Combobox(self.otherFrame, width=10)
        self.blockTex['values'] = self.textures
        self.blockTex['state'] = 'readonly'
        self.blockTex.current(self.textures.index(self.cfg['texture']))
        self.blockTex.grid(row=2, column=1)
        self.blockTex.bind('<<ComboboxSelected>>', self.blockTexChange)
        self.OkBtn = tk.Button(self.frame, text="OK")
        self.OkBtn.bind('<Button-1>', self.closeWindowOk)
        self.CancelBtn = tk.Button(self.frame, text="Cancel")
        self.CancelBtn.bind('<Button-1>', self.closeWindowCancel)

        self.bindFrame.grid(row=0, column=0, columnspan=2, padx=20, pady=20)
        self.otherFrame.grid(row=1, column=0, columnspan=2, padx=20, pady=10, sticky='EW', ipadx=10)
        self.OkBtn.grid(column=0, row=2, padx=5, pady=(5, 15), sticky='E')
        self.CancelBtn.grid(column=1, row=2, padx=5, pady=(5, 15), sticky='W')
        self.frame.pack()

    def closeWindowOk(self, event=None):
        '''write config file then exit the application'''
        if event is not None:
            f = open('config.json', 'w')
            json.dump(self.cfg, f)
            f.close()
            self.root.destroy()

    def closeWindowCancel(self, event):
        self.root.destroy()

    def toggleHold(self):
        self.cfg['enableHold'] = not self.cfg['enableHold']

    def blockTexChange(self, event):
        self.cfg['texture'] = self.blockTex.get()

    def setBinding(self, event, keyAction):
        def getBoundKeys():
            boundKeys = []
            for i in self.cfg['keybinds']:
                boundKeys.append(self.cfg['keybinds'][i]['keysym'])
            return boundKeys

        def closeBindForm(event=None):
            if event is not None:
                if event.keysym == 'Escape':
                    bindForm.destroy()
                elif event.keysym not in getBoundKeys():
                    self.cfg['keybinds'][keyAction]['keysym'] = event.keysym
                    self.cfgWidgets[keyAction]['label']['text'] = '(%s)' % event.keysym
                    bindForm.destroy()
                else:
                    tkmessagebox.showwarning("Key In Use", "The %s key is already bound" % event.keysym)

        bindForm = tk.Toplevel(self.root, width=200, height=200)
        bindForm.protocol('WM_DELETE_WINDOW', closeBindForm)
        bindForm.resizable(0, 0)
        bindForm.bind('<Key>', closeBindForm)
        label = tk.Label(bindForm, text="Press a key for")
        label.grid(row=0, column=0, padx=(40, 0), pady=(20, 0))
        label = tk.Label(bindForm, text=self.cfg['keybinds'][keyAction]['desc'])
        label.grid(row=0, column=1, padx=(5, 40), pady=(20, 0))
        label = tk.Label(bindForm, text="(press Escape to abort)")
        label.grid(row=1, columnspan=2, pady=(0, 20))
        bindForm.update()
        print(keyAction)


def openConfig():
    root = tk.Tk()
    ConfigFrame(root)
    root.title('Configuration')
    root.mainloop()


if __name__ == '__main__':
    openConfig()
