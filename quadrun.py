#!/usr/bin/python

'''
    (C) Copyright 2020  Clinton Hunter
    This file is part of Quaddy Blocks

    Quaddy Blocks is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
    License as published by the Free Software Foundation, either version 3 of the License, or(at your option) any later
    version.

    Quaddy Blocks is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
    details.

    You should have received a copy of the GNU General Public License along with Quaddy Blocks.
    If not, see <https://www.gnu.org/licenses/>.
'''

import tkinter as tk
import tkinter.font as tkfont
import tkinter.messagebox as tkmessagebox
import tkinter.ttk as ttk
import quadscores
import quadconfig
import json
import time
import os
import random


class Quaddy():
    def __init__(self, root, cfg):
        self.root = root
        self.cfg = cfg
        if self.cfg['scale'] not in [1.0, 1.5, 2.0]:
            self.scale = 1.0
        else:
            self.scale = cfg['scale']
        self.tex = self.cfg['texture']
        self.binds = self.cfg['keybinds']
        self.pieceFile = 'pieces.json'
        self.scoreFile = 'highscores.json'
        # gs (short for gridsize)
        self.gs = int(self.scale * 24)
        self.canvasHeight = self.gs * 20
        self.canvasWidth = self.gs * 10
        self.tickRate = 5
        self.pauseEnabled = False
        self.tickCounter = 0
        self.scoreTable = [0, 100, 300, 500, 800, 800, 1200, 1600, 2000]
        self.pieceDict = json.loads(open(self.pieceFile, 'r').read())
        self.highScores = json.loads(open(self.scoreFile, 'r').read())
        self.startLevel = 0
        self.level = 0
        self.levelTicks = 200
        self.gameRunning = False
        self.createWidgets()

    def openScores(self):
        quadscores.ScoreFrame(tk.Toplevel(self.root))

    def gameSettings(self):
        quadconfig.ConfigFrame(tk.Toplevel(self.root))

    def createWidgets(self):
        self.root.resizable(0, 0)
        self.frame = tk.Frame(self.root)
        self.leftPane = tk.Frame(self.frame)
        self.rightPane = tk.Frame(self.frame)
        self.root.bind('<Key>', self.processInput)

        self.blockOver = tk.PhotoImage(file='tex%s%s_%s.png' % (os.sep, self.tex, self.gs))
        self.headFont = (tkfont.nametofont('TkHeadingFont').configure()['family'], int(16 * self.scale), 'bold')
        self.textFont = (tkfont.nametofont('TkFixedFont').configure()['family'], int(14 * self.scale))
        self.helpFont = (tkfont.nametofont('TkFixedFont').configure()['family'], int(12 * self.scale))
        self.statsTitle = (tkfont.nametofont('TkDefaultFont').configure()['family'], int(12 * self.scale), 'bold')
        self.statsFont = (tkfont.nametofont('TkDefaultFont').configure()['family'], int(12 * self.scale))
        self.bannerFont = (tkfont.nametofont('TkDefaultFont').configure()['family'], int(24 * self.scale), 'bold')
        self.countdownFont = (tkfont.nametofont('TkDefaultFont').configure()['family'], int(60 * self.scale), 'bold')

        self.menuBar = tk.Menu(self.root)
        self.fileMenu = tk.Menu(self.menuBar, tearoff=0)
        self.fileMenu.add_command(label='Start New Game', command=self.startGame)
        self.fileMenu.add_command(label='View High Scores', command=self.openScores)
        self.fileMenu.add_command(label='Settings', command=self.gameSettings)
        self.fileMenu.add_command(label='Help', command=self.showGameHelp)
        self.fileMenu.add_command(label='Quit', command=self.closeWindow)
        self.menuBar.add_cascade(label='Game', menu=self.fileMenu)
        self.root.config(menu=self.menuBar)

        self.gameCan = tk.Canvas(self.leftPane, bg='#111111', height=self.canvasHeight, width=self.canvasWidth)
        self.nextCan = tk.Canvas(self.rightPane, bg='#111111', height=self.gs*4, width=self.gs*4)
        self.heldCan = tk.Canvas(self.rightPane, bg='#111111', height=self.gs*4, width=self.gs*4)
        self.label1 = tk.Label(self.rightPane, text='Score', font=self.headFont)
        self.scoreLabel = tk.Label(self.rightPane, text='0', font=self.textFont, width=12)
        self.label2 = tk.Label(self.rightPane, text='Level', font=self.headFont)
        self.levelLabel = tk.Label(self.rightPane, text='0', font=self.textFont)
        self.label3 = tk.Label(self.rightPane, text='Lines', font=self.headFont)
        self.linesLabel = tk.Label(self.rightPane, text='0', font=self.textFont)
        self.label4 = tk.Label(self.rightPane, text='Held', font=self.headFont)
        self.label5 = tk.Label(self.rightPane, text='Next', font=self.headFont)

        self.gameStatus = tk.LabelFrame(self.rightPane, text='Stats', font=self.statsTitle)
        self.lineClearLabels = []
        self.label6 = tk.Label(self.gameStatus, text='Singles', font=self.statsFont)
        self.lineClearLabels.append(tk.Label(self.gameStatus, text='0', width=3, font=self.statsFont))
        self.label7 = tk.Label(self.gameStatus, text='Doubles', font=self.statsFont)
        self.lineClearLabels.append(tk.Label(self.gameStatus, text='0', font=self.statsFont))
        self.label8 = tk.Label(self.gameStatus, text='Triples', font=self.statsFont)
        self.lineClearLabels.append(tk.Label(self.gameStatus, text='0', font=self.statsFont))
        self.label9 = tk.Label(self.gameStatus, text='Quaddles', font=self.statsFont)
        self.lineClearLabels.append(tk.Label(self.gameStatus, text='0', font=self.statsFont))
        self.label10 = tk.Label(self.gameStatus, text='Max Combo', font=self.statsFont)
        self.combosLabel = tk.Label(self.gameStatus, text='0', font=self.statsFont)
        self.label11 = tk.Label(self.gameStatus, text='Perfect Clears', font=self.statsFont)
        self.perfectLabel = tk.Label(self.gameStatus, text='0', font=self.statsFont)
        self.label12 = tk.Label(self.gameStatus, text='PPS', font=self.statsFont)
        self.ppsLabel = tk.Label(self.gameStatus, text='0.00', font=self.statsFont)
        self.sep = ttk.Separator(self.gameStatus)

        self.gameCan.grid(row=0, column=0, padx=(20, 10), pady=(20, 20))
        self.label1.grid(row=0, column=0, padx=5, pady=(20, 0))
        self.scoreLabel.grid(row=0, column=1, padx=5, pady=(20, 0))
        self.label2.grid(row=1, column=0, padx=5, pady=0)
        self.levelLabel.grid(row=1, column=1, padx=5, pady=0)
        self.label3.grid(row=2, column=0, padx=5, pady=(0, 20))
        self.linesLabel.grid(row=2, column=1, padx=5, pady=(0, 20))
        self.label4.grid(row=3, column=0, padx=5, pady=5)
        self.label5.grid(row=3, column=1, padx=5, pady=5)
        self.heldCan.grid(row=4, column=0, padx=10, pady=(0, 20))
        self.nextCan.grid(row=4, column=1, padx=10, pady=(0, 20))

        self.gameStatus.grid(row=5, column=0, columnspan=2, padx=(10, 20), pady=(10, 20), ipady=3, sticky='N')
        self.label6.grid(row=0, column=0, padx=(10, 0), sticky='W')
        for i in range(len(self.lineClearLabels)):
            self.lineClearLabels[i].grid(row=i, column=1, padx=20)
        self.label7.grid(row=1, column=0, padx=(10, 0), sticky='W')
        self.label8.grid(row=2, column=0, padx=(10, 0), sticky='W')
        self.label9.grid(row=3, column=0, padx=(10, 0), sticky='W')
        self.label10.grid(row=4, column=0, padx=(10, 0), sticky='W')
        self.combosLabel.grid(row=4, column=1, padx=20)
        self.label11.grid(row=5, column=0, padx=(10, 0), sticky='W')
        self.perfectLabel.grid(row=5, column=1, padx=20)
        self.sep.grid(row=6, column=0, columnspan=2, sticky='EW', pady=10)
        self.label12.grid(row=7, column=0, padx=(10, 0), sticky='W')
        self.ppsLabel.grid(row=7, column=1, padx=20)

        # draw grid lines to canvas
        for x in range(10):
            line = (self.gs*x, 0, self.gs*x, self.canvasHeight)
            self.gameCan.create_line(line, fill='#333333', tag='grid')
        for y in range(21):
            line = (0, self.gs*y, self.canvasWidth, self.gs*y)
            self.gameCan.create_line(line, fill='#333333', tag='grid')

        self.countText = self.gameCan.create_text(self.canvasWidth // 2, self.canvasHeight // 2)
        self.gameCan.itemconfigure(self.countText, font=self.countdownFont, fill='white', tag='text')
        rect = (0, self.canvasHeight//2 - (self.gs*2), self.canvasWidth, self.canvasHeight//2 + (self.gs*2))
        self.gameBannerRect = self.gameCan.create_rectangle(rect, fill='black')
        self.gameCan.itemconfigure(self.gameBannerRect, state='hidden')
        self.gameBannerText = self.gameCan.create_text(self.canvasWidth // 2, self.canvasHeight // 2)
        self.gameCan.itemconfigure(self.gameBannerText, font=self.bannerFont, fill='white', tag='text')
        self.gameCan.itemconfigure(self.gameBannerText, text='Paused', state='hidden')
        self.gameCan.tkraise(self.gameBannerText)
        self.leftPane.grid(row=0, column=0, padx=20)
        self.rightPane.grid(row=0, column=1, padx=20, sticky='N')
        self.frame.pack()
        self.root.update()
        self.root.geometry('+{}+{}'.format(int(self.root.winfo_screenwidth()/2 - self.root.winfo_width()/2),
                                           int(self.root.winfo_screenheight()/3 - self.root.winfo_height()/3)))

    # game logic and control functions
    def gameloop(self):
        if not self.gameRunning:
            return
        self.tickCounter += 1
        if self.tickCounter == self.levelTicks:
            if self.doesPieceFit(self.activePiece['x'], self.activePiece['y']+1, self.activePiece['rot']):
                self.activePiece['y'] += 1
                self.drawGamePiece('active')
            else:
                time.sleep(0.1)
                self.lockPiece()
            self.tickCounter = 0
        self.root.after(self.tickRate, self.gameloop)

    def startGame(self):
        self.fileMenu.entryconfig('Start New Game', state='disabled')
        countdown = ['3', '2', '1', 'GO!']
        self.gameCan.delete('active')
        self.gameCan.delete('locked')
        self.nextCan.delete('preview')
        self.gameCan.itemconfigure(self.gameBannerRect, state='hidden')
        self.gameCan.itemconfigure(self.gameBannerText, text='Pause', state='hidden')
        self.gameField = [['.' for i in range(10)] for j in range(20)]
        self.pieceBag = list(self.pieceDict.keys())
        self.activePiece = {'id': '', 'x': 0, 'y': 0, 'rot': 0}
        self.previewPiece = self.pieceBag.pop(random.choice(range(len(self.pieceBag))))
        self.heldPiece = None
        self.drawPreviewPiece(self.nextCan, self.previewPiece)
        self.drawPreviewPiece(self.heldCan, self.heldPiece)
        self.gameScore = 0
        self.scoreBonus = 0
        self.lineClearStats = [0, 0, 0, 0, 0, 0]
        for i in self.lineClearLabels:
            i.config(text='0')
        self.combosLabel.config(text='0')
        self.perfectLabel.config(text='0')
        self.linesLabel.config(text='0')
        self.ppsLabel.config(text='0.00')
        self.comboCount = 0
        self.linesCleared = 0
        self.pieceCount = 0
        self.level = self.startLevel
        self.setUpdateSpeed()
        self.tickCounter = 0
        self.pieceLocked = False
        self.pieceSwitched = False
        self.scoreLabel.config(text='0')
        self.levelLabel.config(text=str(self.level))
        self.startGameAnimation(countdown)

    def setUpdateSpeed(self):
        level_zero_speed = 250
        constA = 0.8
        constB = 0.0004
        self.levelTicks = int(round(level_zero_speed * pow(constA - self.level*constB, self.level)))

    def startGameAnimation(self, countdown):
        if len(countdown) > 0:
            self.gameCan.itemconfigure(self.countText, text=countdown.pop(0))
            self.root.after(1000, self.startGameAnimation, countdown)
        else:
            self.gameCan.itemconfigure(self.countText, text='')
            self.gameRunning = True
            self.choosePreviewPiece()
            self.spawnActivePiece()
            self.drawGamePiece('active')
            self.drawPreviewPiece(self.nextCan, self.previewPiece)
            self.startTime = time.time()
            self.pauseEnabled = True
            self.gameloop()

    def choosePreviewPiece(self):
        if len(self.pieceBag) == 0:
            self.pieceBag = list(self.pieceDict.keys())
        self.activePiece['id'] = self.previewPiece
        self.previewPiece = self.pieceBag.pop(random.choice(range(len(self.pieceBag))))

    def spawnActivePiece(self):
        self.activePiece['rot'] = 0
        self.activePiece['x'] = 3
        if self.activePiece['id'] in ['o', 'i']:
            self.activePiece['y'] = -2
        else:
            self.activePiece['y'] = -1
        self.tickCounter = 0

    def rotateBlock(self, sid, rot, px, py):
        '''select one block of a piece based on block coordinates and current rotation'''
        pwidth = self.pieceDict[sid]['width']
        if rot == 0:
            return py*pwidth + px
        elif rot == 1:
            return pwidth*(pwidth-1)+py - pwidth*px
        elif rot == 2:
            return pow(pwidth, 2) - 1 - py*pwidth - px
        elif rot == 3:
            return (pwidth-1-py) + pwidth*px
        else:
            return -1

    def doesPieceFit(self, px, py, pRot):
        pid = self.pieceDict[self.activePiece['id']]
        pwidth = pid['width']
        for y in range(pwidth):
            for x in range(pwidth):
                ind = self.rotateBlock(self.activePiece['id'], pRot, x, y)
                # check wall and floor collisions
                if pid['shape'][ind] != '.':
                    if (px + x) < 0:
                        return False
                    if (px + x > 9):
                        return False
                    if (py + y > 19):
                        return False
                    # check gamefield collisions
                    if py+y < 0:
                        continue
                    if self.gameField[py + y][px + x] != '.':
                        return False
        return True

    def lockPiece(self):
        '''lock the active piece and check for Game Over condition'''
        self.drawGamePiece('locked')
        self.pieceCount += 1
        self.ppsLabel.config(text='{:.2f}'.format(self.pieceCount / (time.time() - self.startTime)))
        # check for lines to clear else lock it in place
        possibleClears = set()
        actPiece = self.pieceDict[self.activePiece['id']]
        pwidth = actPiece['width']
        for y in range(pwidth):
            for x in range(pwidth):
                currSquare = actPiece['shape'][self.rotateBlock(self.activePiece['id'], self.activePiece['rot'], x, y)]
                if currSquare != '.' and (y + self.activePiece['y'] >= 0):
                    possibleClears.add(y + self.activePiece['y'])
        linesRemoved = 0
        fullLines = []
        for i in possibleClears:
            lineFilled = True
            for x in range(10):
                if self.gameField[i][x] == '.':
                    lineFilled = False
                    break
            if lineFilled:
                fullLines.append(i)
        fullLines = sorted(fullLines)
        for i in fullLines:
            self.gameField.pop(i)
            self.gameField.insert(0, ['.']*10)
            linesRemoved += 1

        if linesRemoved > 0:
            self.lineClearStats[linesRemoved] += 1
            self.lineClearLabels[linesRemoved-1].config(text=str(self.lineClearStats[linesRemoved]))
            self.gameScore += self.scoreTable[linesRemoved] * (self.level+1)
            self.linesCleared += linesRemoved
            self.redrawGameField(1, fullLines)
            self.comboCount += 1
        else:
            self.comboCount = 0

        # check for all blocks cleared
        checkPerfect = list(set([block for line in self.gameField for block in line]))
        if len(checkPerfect) == 1 and checkPerfect[0] == '.':
            self.scoreBonus += self.scoreTable[4+linesRemoved] * (self.level + 1)
            self.lineClearStats[0] += 1
            self.perfectLabel.config(text=str(self.lineClearStats[0]))

        # get next piece / next preview piece / check levelLabel / update score
        self.pieceSwitched = False
        self.choosePreviewPiece()
        self.spawnActivePiece()
        if self.comboCount > 1:
            self.scoreBonus += 50*(self.level+1) * self.comboCount
            if self.lineClearStats[5] < self.comboCount:
                self.lineClearStats[5] = self.comboCount
                self.combosLabel.config(text=str(self.lineClearStats[5]))

        self.gameScore += self.scoreBonus
        self.scoreLabel.config(text='{:,}'.format(self.gameScore))
        self.linesLabel.config(text=str(self.linesCleared))

        if self.doesPieceFit(self.activePiece['x'], self.activePiece['y'], self.activePiece['rot']):
            self.drawGamePiece('active')
            self.drawPreviewPiece(self.nextCan, self.previewPiece)
            self.scoreBonus = 0
            if (self.linesCleared >= (self.level + 1) * 10):
                self.level += 1
                self.setUpdateSpeed()
            self.levelLabel.config(text=str(self.level))
        else:
            self.gameOver()

    def gameOver(self):
        self.fileMenu.entryconfig('Start New Game', state='normal')
        self.drawGamePiece('active')
        self.drawPreviewPiece(self.nextCan, self.previewPiece)
        self.gameRunning = False
        self.pauseEnabled = False
        self.gameCan.itemconfigure(self.gameBannerRect, state='normal')
        self.gameCan.itemconfigure(self.gameBannerText, text='Game Over', state='normal')
        self.gameCan.tkraise(self.gameBannerRect)
        self.gameCan.tkraise(self.gameBannerText)
        # check for high score
        if self.gameScore > min(self.highScores[1]):
            for i in range(len(self.highScores[1])):
                if self.gameScore > self.highScores[1][i]:
                    self.highScores[1].insert(i, self.gameScore)
                    self.setScore(i)
                    break

    def setScore(self, scorePosIndex):
        def closeForm(event=None):
            if event is not None:
                self.highScores[0].insert(scorePosIndex, nameEntry.get())
                self.highScores[0].pop()
                self.highScores[1].pop()
                f = open(self.scoreFile, 'w')
                json.dump(self.highScores, f)
                f.close()
                scoreForm.destroy()
                quadscores.openScores()

        scoreForm = tk.Toplevel(self.root, width=300, height=150)
        scoreForm.resizable(0, 0)
        label1 = tk.Label(scoreForm, text='Enter your name', width=17, font=self.headFont)
        nameEntry = tk.Entry(scoreForm, width=15, font=self.textFont)
        submitBtn = tk.Button(scoreForm, text='Submit', font=self.textFont)
        submitBtn.bind('<Button-1>', closeForm)
        label1.grid(row=0, column=0, padx=30, pady=10)
        nameEntry.grid(row=1, column=0, pady=10)
        submitBtn.grid(row=2, column=0, pady=10)
        scoreForm.wm_title('New High Score!')
        scoreForm.protocol('WM_DELETE_WINDOW', closeForm)

    def closeWindow(self):
        '''call to exit the application'''
        self.root.destroy()

    # user input functions
    def processInput(self, event):
        if event.keysym == self.binds['moveLeft']['keysym']:
            self.moveLeft()
        elif event.keysym == self.binds['moveRight']['keysym']:
            self.moveRight()
        elif event.keysym == self.binds['moveDown']['keysym']:
            self.moveDown()
        elif event.keysym == self.binds['rotateCW']['keysym']:
            self.rotateCW()
        elif event.keysym == self.binds['rotateCCW']['keysym']:
            self.rotateCCW()
        elif event.keysym == self.binds['hardDrop']['keysym']:
            self.hardDrop()
        elif event.keysym == self.binds['pauseGame']['keysym']:
            self.pauseGame()
        elif event.keysym == self.binds['switchPiece']['keysym']:
            self.switchPiece()
        elif event.keysym == self.binds['levelUp']['keysym']:
            self.levelUp()
        elif event.keysym == self.binds['levelDown']['keysym']:
            self.levelDown()

    def moveLeft(self):
        if self.gameRunning and \
                self.doesPieceFit(self.activePiece['x']-1, self.activePiece['y'], self.activePiece['rot']):
            self.activePiece['x'] -= 1
            self.drawGamePiece('active')
            self.tickCounter -= int(0.8 * self.level)

    def moveRight(self):
        if self.gameRunning and \
                self.doesPieceFit(self.activePiece['x']+1, self.activePiece['y'], self.activePiece['rot']):
            self.activePiece['x'] += 1
            self.drawGamePiece('active')
            self.tickCounter -= int(0.8 * self.level)

    def moveDown(self):
        if self.gameRunning:
            if self.doesPieceFit(self.activePiece['x'], self.activePiece['y']+1, self.activePiece['rot']):
                self.activePiece['y'] += 1
                self.drawGamePiece('active')
                self.tickCounter = 0
                self.scoreBonus += 1
            else:
                self.lockPiece()

    def hardDrop(self):
        if not self.gameRunning:
            return
        y = self.activePiece['y']
        while self.doesPieceFit(self.activePiece['x'], y, self.activePiece['rot']):
            y += 1
        self.scoreBonus += 2 * (y - self.activePiece['y'])
        self.activePiece['y'] = y - 1
        self.drawGamePiece('active')
        self.tickCounter = 0
        self.lockPiece()

    def rotateActivePiece(self, dir):
        if self.gameRunning and \
           self.doesPieceFit(self.activePiece['x'], self.activePiece['y'], (self.activePiece['rot']+dir) % 4):
            self.activePiece['rot'] = (self.activePiece['rot'] + dir) % 4
            self.tickCounter -= int(0.6 * self.level)
            self.drawGamePiece('active')

    def rotateCW(self):
        self.rotateActivePiece(1)

    def rotateCCW(self):
        self.rotateActivePiece(-1)

    def pauseGame(self):
        if not self.pauseEnabled:
            return
        self.gameRunning = not self.gameRunning
        if self.gameRunning:
            self.gameCan.itemconfigure(self.gameBannerRect, state='hidden')
            self.gameCan.itemconfigure(self.gameBannerText, state='hidden')
            self.root.after(self.tickRate, self.gameloop)
        else:
            self.gameCan.itemconfigure(self.gameBannerRect, state='normal')
            self.gameCan.itemconfigure(self.gameBannerText, state='normal')
            self.gameCan.tkraise(self.gameBannerRect)
            self.gameCan.tkraise(self.gameBannerText)

    def switchPiece(self):
        if not self.gameRunning or self.cfg['enableHold'] is False:
            return
        if self.pieceSwitched:
            return
        if self.heldPiece is None:
            self.heldPiece = self.activePiece['id']
            self.activePiece['id'] = self.previewPiece
            self.choosePreviewPiece()
            self.drawPreviewPiece(self.nextCan, self.previewPiece)
        else:
            tmp = self.activePiece['id']
            self.activePiece['id'] = self.heldPiece
            self.heldPiece = tmp

        self.spawnActivePiece()
        self.drawGamePiece('active')
        self.drawPreviewPiece(self.heldCan, self.heldPiece)
        self.pieceSwitched = True
        self.tickCounter = 0

    def levelUp(self):
        if not self.gameRunning:
            self.startLevel += ((self.startLevel+1) < 20)
            self.levelLabel.config(text=str(self.startLevel))

    def levelDown(self):
        if not self.gameRunning:
            self.startLevel -= ((self.startLevel-1) >= 0)
            self.levelLabel.config(text=str(self.startLevel))

    def showGameHelp(self):
        infoForm = tk.Toplevel(self.root, width=300, height=150)
        infoForm.resizable(0, 0)
        infoText = tk.Text(infoForm, font=self.helpFont)
        infoText.grid(row=0, column=0, padx=30, pady=30)
        infoText.insert(tk.INSERT, open('help.txt', 'r').read())
        infoText.config(state='disabled')
        infoForm.wm_title('Help and About Information')

    # game drawing functions

    def drawPreviewPiece(self, canvas, piece):
        offsetx = 0
        offsety = 0
        canvas.delete('preview')
        if piece is None:
            return
        prevPiece = self.pieceDict[piece]
        pwidth = self.pieceDict[piece]['width']
        if piece == 'i':
            offsety = -self.gs // 2
        elif pwidth == 3:
            offsetx = self.gs // 2
            offsety = self.gs
        for y in range(pwidth):
            for x in range(pwidth):
                currSquare = prevPiece['shape'][self.rotateBlock(piece, 0, x, y)]
                if currSquare != '.':
                    rect = (self.gs*x + offsetx, self.gs*y + offsety,
                            self.gs*x + self.gs + offsetx, self.gs*y + self.gs + offsety)
                    canvas.create_rectangle(rect, width=0, fill=prevPiece['color'], tag='preview')

    def drawGamePiece(self, tag):
        if self.gameRunning:
            actPiece = self.pieceDict[self.activePiece['id']]
            pwidth = actPiece['width']
            if tag == 'active':
                self.gameCan.delete('active')
            for y in range(pwidth):
                for x in range(pwidth):
                    currSquare = actPiece['shape'][self.rotateBlock(self.activePiece['id'],
                                                   self.activePiece['rot'], x, y)]
                    if currSquare == '.':
                        continue
                    rect = (self.gs*(self.activePiece['x']+x), self.gs*(self.activePiece['y']+y),
                            self.gs*(self.activePiece['x']+x) + self.gs, self.gs*(self.activePiece['y']+y) + self.gs)
                    imagecoord = (self.gs*(self.activePiece['x']+x), self.gs*(self.activePiece['y']+y))
                    if tag == 'locked':
                        self.gameField[self.activePiece['y']+y][self.activePiece['x']+x] = self.activePiece['id']
                        self.gameCan.create_rectangle(rect, width=0, fill=actPiece['color'], tag=tag)
                        self.gameCan.create_image(imagecoord, image=self.blockOver, tag=tag, anchor='nw')
                    else:
                        self.gameCan.create_rectangle(rect, width=0, fill=actPiece['color'], tag=tag)
                        self.gameCan.create_image(imagecoord, image=self.blockOver, tag=tag, anchor='nw')

    def redrawGameField(self, animSeq, lines):
        if animSeq > 0:
            for i in lines:
                rect = (0, self.gs*i, 10*self.gs, self.gs*i + self.gs)
                self.gameCan.create_rectangle(rect, outline='white', width=0, fill='white', tag='locked')
            self.root.after(100, self.redrawGameField, animSeq-1, lines)
        elif animSeq == 0:
            self.gameCan.delete('locked')
            for y in range(20):
                for x in range(10):
                    if self.gameField[y][x] == '.':
                        continue
                    rect = (self.gs*x, self.gs*y, self.gs*x + self.gs, self.gs*y + self.gs)
                    imagecoord = (self.gs*x, self.gs*y)
                    color = self.pieceDict[self.gameField[y][x]]['color']
                    self.gameCan.create_rectangle(rect, width=0, fill=color, tag='locked')
                    self.gameCan.create_image(imagecoord, image=self.blockOver, tag='locked', anchor='nw')


def loadConfig():
    try:
        config = json.loads(open('config.json', 'r').read())
    except Exception:
        tkmessagebox.showwarning(title="Problem loading config",
                                 message="config.json will be recreated with default values")
        config = {
          "texture": "pyramid", "scale": 1.0, "enableHold": False,
          "keybinds": {
            "moveLeft": "Left", "moveRight": "Right", "rotateActive": "Up", "moveDown": "Down",
            "hardDrop": "space", "switchPiece": "c", "levelDown": "a", "levelUp": "d", "pauseGame": "p"
          }
        }
        f = open('config.json', 'w')
        json.dump(config, f)
        f.close()
    return config


if __name__ == '__main__':
    root = tk.Tk()
    Quaddy(root, cfg=loadConfig())
    root.title('Quaddy Blocks!')
    root.mainloop()
