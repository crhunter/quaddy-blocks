# Quaddy Blocks! v1.4

It's like that other puzzle game you have played before.

## Description

This is a Tetris clone created in Python 3.x.  It uses Tkinter for the GUI and only native python modules, so it should run as is on Linux, Windows and MacOS.
If you don't have Python, you can download the latest Python installer from <https://www.python.org/downloads/>

## TO-DO list

* Have game settings update when the configuration is changed rather than having to close/reopen the game.
* Investigate adding other game modes (Sprint etc)

## Update History

### Version 1.4

**Feature Changes:**

* added in-game dialog to change the configuration file (key bindings, textures etc).  Currently requires a restart to load the new configuration.

### Version 1.3

**Bugfixes:**
* corrected alignment issue in help.txt (mixed spaces and tabs)

**Feature Changes:**
* game configuration now stored in a JSON file, and is loaded when game is launched
* a piece can now be held (disabled by default)
* updated help.txt to include new hold piece function

### Version 1.2

**Feature Changes:**
* block textures added (adjustable in source code only)
* scaling at 100%, 150% and 200% (adjustable in source code only)

### Version 1.1
**Bugfixes:**
* Harddrop clipping through pieces to empty space

**Feature Changes:**
* line clear stats
* scoring for perfect clears
* PPS (speed stat - pieces per second)
* Label displayed when game is paused.
* short delay before next piece if not hard or soft dropped

### Version 1.0
 * 1st release


## Instructions for Use after Downloading

### Starting the game
* Run the quadrun.py file in Python 3... simples!  :-)

### Game Controls
* Move piece left:  Left Arrow
* Move piece right:  Right Arrow
* Move piece down (soft drop):  Down Arrow
* Rotate piece (CW):  Up Arrow
* Hard Drop:   Spacebar
* Pause game:  P

Changing the Level before starting the game:

* Increase starting level: D
* Decrease starting level: A

## Licence

(C) Copyright 2020  Clinton Hunter

Quaddy Blocks is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Quaddy Blocks is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Quaddy Blocks.  If not, see <https://www.gnu.org/licenses/>.
